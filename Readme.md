[![pipeline status](https://gitlab.com/comedian780/gitlab-cicd-templates/badges/master/pipeline.svg)](https://gitlab.com/comedian780/gitlab-cicd-templates/commits/master)

# Gitlab CI/CD Templates
Templates for easier usage and management of pipeline build and test steps in Gitlab CI/CD  

The templates are structured in logical units (interactions with Docker can each be found in the directory [docker](./docker)).  
The template files are written without a trailing dot. Each directory contains an example file that includes the local template files and demonstrates which additional templates or environment variables are necessary to use the provided templates.  
These files have a trailing dot and are tested within this repositories pipeline.  
```sh
# template file you can include:
template.yml
# example that includes template.yml and shows possible configurations 
.example.yml
```

The steps used inside the example files are matched to the according template file.
```yaml
include:
  - remote: 'https://gitlab.com/comedian780/gitlab-cicd-templates/raw/master/telegram/notify.yml'

stages:
  - notify

# job that uses the template written with underscores
telegram-message-to-chat:
  stage: notify
  extends:
    # template that is included
    - .telegram_message_to_chat
```